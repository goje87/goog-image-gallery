import {html, PolymerElement} from '@polymer/polymer/polymer-element.js'

window.customElements.define('gallery-img', class extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: inline-block;
          position: relative;
        }

        .image {
          width: 100%;
          height: 100%
        }

        .placeholder-wrapper {
          position: absolute;
          top: 0;
          left: 0;
          right: 0;
          bottom: 0;
          color: #000;
          text-shadow: 1px 1px 0 #fff;
          font-size: 35pt;
          opacity: 0.5;
          z-index: -1;
          display: flex;
          flex-direction: row;
          align-items: center;
          justify-content: center;
        }
      </style>
      <div class="placeholder-wrapper">
        <div class="placeholder">...</div>
      </div>
    `
  }
  static get properties() {
    return {
      image: {
        type: Object
      },

      width: {
        type: Number,
        value: 300
      },

      height: {
        type: Number,
        value: 250
      },

      fit: {
        type: String,
        value: 'contain'
      }
    }
  }

  static get observers () {
    return [
      '_imageDataChanged(image, width, height)'
    ]
  }

  connectedCallback () {
    super.connectedCallback()

    this.dispatchEvent(new CustomEvent('gallery-img-connected'))
  }

  _imageDataChanged () {
    let pixel = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'
    let { width, height, image } = this

    if (typeof width === 'undefined') width = image.width
    if (typeof height === 'undefined') height = image.height

    let src = this.src = `https://picsum.photos/${width}/${height}?image=${image.id}`
    let imageEl = document.createElement('img')

    imageEl.src = src
    imageEl.className = 'image'
    imageEl.style.objectFit = this.fit

    let oldImageEl = this.shadowRoot.querySelector('.image')
    oldImageEl ? oldImageEl.remove() : ''

    this.shadowRoot.appendChild(imageEl)
  }
})
