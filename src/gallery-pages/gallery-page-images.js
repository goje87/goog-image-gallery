import {html, PolymerElement} from '@polymer/polymer/polymer-element.js'
import '@polymer/polymer/lib/elements/dom-repeat.js'
import '../gallery-img/gallery-img.js'

window.customElements.define('gallery-page-images', class extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          padding: 10px;
          text-align: center;
        }

        .gallery-images {
          display: flex;
          flex-wrap: wrap;
          justify-content: center;
        }

        .gallery-image-container {
          margin: 8px;
          width: 140px;
          height: 100px;
          box-shadow: inset 0 0 1px rgba(0, 0, 0, 0.4);
          background-size: cover;
        }
      </style>
      <div class="gallery-images">
        <template is="dom-repeat" items="[[images]]">
          <gallery-img class="gallery-image-container" 
              fit="cover" 
              width="140" 
              height="100" 
              image="[[item]]" 
              on-click="openImageDetails"
              on-gallery-img-connected="_handleConnectedImg"
          ></gallery-img>
        </template>
      </div>
    `
  }

  _handleConnectedImg (ev) {
    ev.model.set('item.domNode', ev.target)
  }

  openImageDetails (ev) {
    let id = ev.model.item.id
    this.dispatchEvent(new CustomEvent('gallery-page-images-show-details', { detail: { id }}))
  }

  getBoundingBoxForImage (id) {
    let image = this.images[id]

    if (!image) return

    if (image.domNode && image.domNode.getBoundingClientRect) return image.domNode.getBoundingClientRect()
  }
})