import { html, PolymerElement } from "../../node_modules/@polymer/polymer/polymer-element.js";
import '../gallery-pages/gallery-page-images.js';
import '../gallery-image-details/gallery-image-details.js';
/**
 * @customElement
 * @polymer
 */

class GoogImageGalleryApp extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          height: 100vh;
          width: 100vw;
          overflow: hidden;
          display: flex;
          flex-direction: column;
        }

        .header {
          height: 60px;
          border-bottom: 1px solid #e3e3e3;
          background-color: #f5f5f5;
          text-align: center;
          line-height: 60px;
        }

        .header h2 {
          font-family: Roboto, Arial;
          font-size: 20pt;
          font-weight: normal;
          padding: 0;
          margin: 0;
        }

        .body {
          flex: 1;
          flex-basis: 0.000000001px;
          overflow: auto;
        }
      </style>
      <div class="header">
        <h2>Image gallery</h2>
      </div>
      <div class="body">
        <gallery-page-images id="page-images" images="[[images]]" on-gallery-page-images-show-details="_handleOpenImageDetails"></gallery-page-images>
      </div>
      <div>
        <gallery-image-details id="details-dialog" 
            image="[[currentImage]]" 
            prev-image="[[prevImage]]" 
            next-image="[[nextImage]]"
            on-gallery-image-details-change="_handleImageChange"
        ></gallery-image-details>
      </div>
    `;
  }

  static get properties() {
    return {
      images: {
        type: Array,
        value: () => [{
          format: "jpeg",
          width: 5616,
          height: 3744,
          filename: "0000_yC-Yzbqy7PY.jpeg",
          id: 0,
          author: "Alejandro Escamilla",
          author_url: "https://unsplash.com/@alejandroescamilla",
          post_url: "https://unsplash.com/photos/yC-Yzbqy7PY"
        }, {
          format: "jpeg",
          width: 5616,
          height: 3744,
          filename: "0001_LNRyGwIJr5c.jpeg",
          id: 1,
          author: "Alejandro Escamilla",
          author_url: "https://unsplash.com/@alejandroescamilla",
          post_url: "https://unsplash.com/photos/LNRyGwIJr5c"
        }, {
          format: "jpeg",
          width: 5616,
          height: 3744,
          filename: "0002_N7XodRrbzS0.jpeg",
          id: 2,
          author: "Alejandro Escamilla",
          author_url: "https://unsplash.com/@alejandroescamilla",
          post_url: "https://unsplash.com/photos/N7XodRrbzS0"
        }, {
          format: "jpeg",
          width: 5616,
          height: 3744,
          filename: "0003_Dl6jeyfihLk.jpeg",
          id: 3,
          author: "Alejandro Escamilla",
          author_url: "https://unsplash.com/@alejandroescamilla",
          post_url: "https://unsplash.com/photos/Dl6jeyfihLk"
        }, {
          format: "jpeg",
          width: 5616,
          height: 3744,
          filename: "0004_y83Je1OC6Wc.jpeg",
          id: 4,
          author: "Alejandro Escamilla",
          author_url: "https://unsplash.com/@alejandroescamilla",
          post_url: "https://unsplash.com/photos/y83Je1OC6Wc"
        }, {
          format: "jpeg",
          width: 5245,
          height: 3497,
          filename: "0005_LF8gK8-HGSg.jpeg",
          id: 5,
          author: "Alejandro Escamilla",
          author_url: "https://unsplash.com/@alejandroescamilla",
          post_url: "https://unsplash.com/photos/LF8gK8-HGSg"
        }, {
          format: "jpeg",
          width: 5616,
          height: 3744,
          filename: "0006_tAKXap853rY.jpeg",
          id: 6,
          author: "Alejandro Escamilla",
          author_url: "https://unsplash.com/@alejandroescamilla",
          post_url: "https://unsplash.com/photos/tAKXap853rY"
        }, {
          format: "jpeg",
          width: 4728,
          height: 3168,
          filename: "0007_BbQLHCpVUqA.jpeg",
          id: 7,
          author: "Alejandro Escamilla",
          author_url: "https://unsplash.com/@alejandroescamilla",
          post_url: "https://unsplash.com/photos/BbQLHCpVUqA"
        }, {
          format: "jpeg",
          width: 5616,
          height: 3744,
          filename: "0008_xII7efH1G6o.jpeg",
          id: 8,
          author: "Alejandro Escamilla",
          author_url: "https://unsplash.com/@alejandroescamilla",
          post_url: "https://unsplash.com/photos/xII7efH1G6o"
        }, {
          format: "jpeg",
          width: 5616,
          height: 3672,
          filename: "0009_ABDTiLqDhJA.jpeg",
          id: 9,
          author: "Alejandro Escamilla",
          author_url: "https://unsplash.com/@alejandroescamilla",
          post_url: "https://unsplash.com/photos/ABDTiLqDhJA"
        }, {
          format: "jpeg",
          width: 2500,
          height: 1667,
          filename: "0010_6J--NXulQCs.jpeg",
          id: 10,
          author: "Paul Jarvis",
          author_url: "https://unsplash.com/@pjrvs",
          post_url: "https://unsplash.com/photos/6J--NXulQCs"
        }, {
          format: "jpeg",
          width: 2500,
          height: 1667,
          filename: "0011_Cm7oKel-X2Q.jpeg",
          id: 11,
          author: "Paul Jarvis",
          author_url: "https://unsplash.com/@pjrvs",
          post_url: "https://unsplash.com/photos/Cm7oKel-X2Q"
        }, {
          format: "jpeg",
          width: 2500,
          height: 1667,
          filename: "0012_I_9ILwtsl_k.jpeg",
          id: 12,
          author: "Paul Jarvis",
          author_url: "https://unsplash.com/@pjrvs",
          post_url: "https://unsplash.com/photos/I_9ILwtsl_k"
        }, {
          format: "jpeg",
          width: 2500,
          height: 1667,
          filename: "0013_3MtiSMdnoCo.jpeg",
          id: 13,
          author: "Paul Jarvis",
          author_url: "https://unsplash.com/@pjrvs",
          post_url: "https://unsplash.com/photos/3MtiSMdnoCo"
        }, {
          format: "jpeg",
          width: 2500,
          height: 1667,
          filename: "0014_IQ1kOQTJrOQ.jpeg",
          id: 14,
          author: "Paul Jarvis",
          author_url: "https://unsplash.com/@pjrvs",
          post_url: "https://unsplash.com/photos/IQ1kOQTJrOQ"
        }, {
          format: "jpeg",
          width: 2500,
          height: 1667,
          filename: "0015_NYDo21ssGao.jpeg",
          id: 15,
          author: "Paul Jarvis",
          author_url: "https://unsplash.com/@pjrvs",
          post_url: "https://unsplash.com/photos/NYDo21ssGao"
        }]
      }
    };
  }

  static get observers() {
    '_imagesChanged(images.*)';
  }

  _handleOpenImageDetails(ev) {
    this.openImageDetails(ev.detail.id);
  }

  openImageDetails(id) {
    this.currentImage = this.images[id];
    this.prevImage = id > 0 ? this.images[id - 1] : undefined;
    this.nextImage = id < this.images.length ? this.images[id + 1] : undefined;
    let boundingBox = this.$['page-images'].getBoundingBoxForImage(id);
    let {
      left,
      width,
      top,
      height
    } = boundingBox;
    let x = left + width / 2;
    let y = top + height / 2;
    this.$['details-dialog'].open(x, y);
  }

  _handleImageChange(ev) {
    this.openImageDetails(ev.detail.id);
  }

}

window.customElements.define('goog-image-gallery-app', GoogImageGalleryApp);