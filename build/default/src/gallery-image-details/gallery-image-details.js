import { html, PolymerElement } from "../../node_modules/@polymer/polymer/polymer-element.js";
import './gallery-image-slider.js';
window.customElements.define('gallery-image-details', class extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }

        #container {
          height: 100vh;
          width: 100vw;
          overflow: hidden;
          position: fixed;
          top: 0;
          left: 0;
          background-color: #000;
          color: #fff;
          transform: scale(0);
          transition: transform 300ms ease-out;
        }

        #container.opened {
          transform: scale(1);
        }

        .tools {
          opacity: 1;
          transition: opacity 200ms ease-out;
        }

        .tools.hidden {
          opacity: 0;
        }

        .tools .header {
          position: absolute;
          top: 0;
          left: 0;
          right: 0;
          height: 60px;
          background-color: #333;
          color: #ebebeb;
          text-align: center;
          line-height: 60px;
          display: flex;
          flex-direction: row;
        }

        .tools .header .title {
          flex: 1;
          flex-basis: 0.000000001px;
        }

        .tools .header .actions {
          display: flex;
          flex-direction: row;
        }

        .tools .icon {
          display: block;
          cursor: pointer;
        }

        .tools .header .icon {
          width: 14px;
          height: 14px; 
          padding: calc((60px - 14px) / 2);
        }

        .tools .footer {
          display: flex;
          flex-direction: row;
          justify-content: space-around;
          height: 45px;
          position: absolute;
          bottom: 0;
          left: 0;
          width: 100vw;
          background-color: #333;
          color: #EBEBEB;
        }

        .tools .footer .icon {
          width: 20px;
          height: 20px;
          padding: calc((45px - 20px) / 2);
        }

        .image-slider {
          width: 100%;
          height: 100%;
        }
      </style>
      <div id="container">
        <gallery-image-slider on-click="toggleTools" class="image-slider" 
            image="[[image]]" 
            prev-image="[[prevImage]]" 
            next-image="[[nextImage]]"
            on-gallery-image-slider-change="_handleSlidImage"
        ></gallery-image-slider>
        <div class="tools">
          <div class="header">
            <div class="title">
              [[image.filename]]
            </div>
            <div class="actions">
                <img class="icon" on-click="close" src="./images/close.svg" />
            </div>
          </div>
          <div class="footer">
            <img class="icon" src="images/plus-one.svg" />
            <img class="icon" src="images/comment.svg" />
            <img class="icon" src="images/plus.svg" />
            <img class="icon" src="images/share.svg" />
          </div>
        </div>
      </div>
    `;
  }

  static get properties() {
    return {
      image: {
        type: String,
        value: ''
      },
      opened: {
        type: Boolean,
        value: false
      }
    };
  }

  static get observers() {
    return ['_openStateChanged(opened)'];
  }

  open(x, y) {
    if (x || y) {
      let container = this.$['container'];
      container.style.transformOrigin = `${x}px ${y}px`;
    }

    this.opened = true;
  }

  close() {
    this.opened = false;
  }

  _openStateChanged() {
    let classList = this.$['container'].classList;

    if (this.opened) {
      classList.add('opened');
    } else {
      classList.remove('opened');
    }
  }

  toggleTools() {
    let toolsContainer = this.shadowRoot.querySelector('.tools');
    toolsContainer.classList.toggle('hidden');
  }

  _handleSlidImage(ev) {
    this.dispatchEvent(new CustomEvent('gallery-image-details-change', {
      detail: ev.detail
    }));
  }

});