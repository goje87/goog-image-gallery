import { html, PolymerElement } from "../../node_modules/@polymer/polymer/polymer-element.js";
import "../../node_modules/@polymer/polymer/lib/elements/dom-if.js";
import '../gallery-img/gallery-img.js';
window.customElements.define('gallery-image-slider', class extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          overflow: hidden;
        }

        .image-strip {
          width: 100%;
          height: 100%;
          position: relative;
          transition: transform 0 ease-out;
        }

        .image-strip.no-animation {
          transition: none;
        }

        .image {
          width: 100%;
          height: 100%;
          object-fit: contain;
        }

        .image.prev {
          position: absolute;
          top: 0;
          right: 100%;
        }

        .image.next {
          position: absolute;
          top: 0;
          left: 100%;
        }
      </style>
      <div class="image-strip" on-touchstart="_handleTouchStart" on-touchmove="_handleTouchMove" on-touchend="_handleTouchEnd">
        <template is="dom-if" if="[[prevImage]]">
          <gallery-img class="image prev" image="[[prevImage]]" fit="contain"></gallery-img>
        </template>
        <gallery-img class="image" image="[[image]]" fit="contain"></gallery-img>
        <template is="dom-if" if="[[nextImage]]">
          <gallery-img class="image next" image="[[nextImage]]" fit="contain"></gallery-img>
        </template>
      </div>
    `;
  }

  static get properties() {
    return {
      image: {
        type: String,
        value: ''
      }
    };
  }

  static get observers() {
    return ['_imageChanged(image)'];
  }

  _imageChanged() {
    if (this._strip) {
      this._translateStrip('0');
    }
  }

  connectedCallback() {
    super.connectedCallback();
    this._strip = this.shadowRoot.querySelector('.image-strip');
  }

  _getX(touchEvent) {
    return touchEvent.changedTouches[0].clientX;
  }

  _handleTouchStart(ev) {
    this._isSliding = true;
    this._firstTouch = ev;
    this._prevTouch = ev;
    this._velocity = null;
  }

  _handleTouchMove(ev) {
    if (!this._isSliding) return;
    ev.preventDefault();
    let fTouch = this._firstTouch;
    let pTouch = this._prevTouch;
    let cTouch = ev;

    let cx = this._getX(cTouch);

    if (fTouch) {
      let fx = this._getX(fTouch);

      let dx = cx - fx;

      this._translateStrip(`${dx}px`);
    }

    if (pTouch) {
      let px = this._getX(pTouch);

      let dx = cx - px;
      let dt = cTouch.timeStamp - pTouch.timeStamp;
      this._velocity = dx / dt;
    }

    this._prevTouch = ev;
  }

  _handleTouchEnd(ev) {
    if (!this._isSliding) return;
    const absThresholdVelocity = 1;
    const absDisplacementPercent = 0.6;

    if (Math.abs(this._velocity) >= absThresholdVelocity) {
      if (this._velocity > 0) {
        this.slideToPrev();
      } else {
        this.slideToNext();
      }
    } else {
      let {
        x,
        width
      } = this._strip.getBoundingClientRect();

      let dPercent = x / width;

      if (Math.abs(dPercent) >= absDisplacementPercent) {
        if (dPercent > 0) {
          this.slideToPrev();
        } else {
          this.slideToNext();
        }
      } else {
        this.slideToCurrent();
      }
    }

    this._isSliding = false;
  }

  _translateStrip(value, duration = 0) {
    let strip = this._strip;
    if (!strip) return Promise.resolve();
    return new Promise((resolve, reject) => {
      strip.style.transitionDuration = `${duration}ms`;
      strip.addEventListener('transitionend', resolve, {
        once: true
      });
      strip.style.transform = `translateX(${value})`;
      if (duration <= 0) resolve();
    });
  }

  slideToNext() {
    if (!(this.nextImage && typeof this.nextImage.id !== 'undefined')) {
      this.slideToCurrent();
      return;
    }

    this._translateStrip('-100%', 100).then(() => {
      this.dispatchEvent(new CustomEvent('gallery-image-slider-change', {
        detail: this.nextImage
      }));
    });
  }

  slideToPrev() {
    if (!(this.prevImage && typeof this.prevImage.id !== 'undefined')) {
      this.slideToCurrent();
      return;
    }

    this._translateStrip('100%', 100).then(() => {
      this.dispatchEvent(new CustomEvent('gallery-image-slider-change', {
        detail: this.prevImage
      }));
    });
  }

  slideToCurrent() {
    this._translateStrip('0', 100);
  }

});